package logosProject;

import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.FeatureDetector;

public class KeypointsExtractor {//template matching analasys creator 
	
	private static FeatureDetector siftDetector;
	private static DescriptorExtractor siftExtractor;
	static {
		siftDetector = FeatureDetector.create(FeatureDetector.SIFT);
		siftExtractor= DescriptorExtractor.create(DescriptorExtractor.SIFT);
	}
	
	public static KeypointsStruct getKeypointsStruct(Mat img){		
		MatOfKeyPoint imgKeyPoints = new MatOfKeyPoint();
		siftDetector.detect(img, imgKeyPoints);
		
		Mat imgDescriptors = img.clone();
		siftExtractor.compute(img, imgKeyPoints, imgDescriptors);
		
		return new KeypointsStruct(imgKeyPoints,imgDescriptors);
	}
	public static Mat getDescriptors(Mat img){		
		MatOfKeyPoint imgKeyPoints = new MatOfKeyPoint();
		siftDetector.detect(img, imgKeyPoints);
		Mat imgDescriptors = img.clone();
		siftExtractor.compute(img, imgKeyPoints, imgDescriptors);
		
		return imgDescriptors;
	}
	
	public static class KeypointsStruct{
		
		MatOfKeyPoint keypoints;
		Mat descriptors;
		
		public KeypointsStruct(MatOfKeyPoint keypoints, Mat descriptors) {
			this.keypoints = keypoints;
			this.descriptors = descriptors;	
		}
			
	}
		
}
	
