package logosProject;

import java.util.List;
import java.util.Vector;

import org.opencv.core.Core;
import org.opencv.core.DMatch;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Features2d;
import org.opencv.imgcodecs.Imgcodecs;

import logosProject.KeypointsExtractor.KeypointsStruct;


public class siftTest {
	//static final String templatePath = "/home/klein/dev/logosProject/tests/images/box.pgm";
	//static final String imagePath = "/home/klein/dev/logosProject/tests/images/scene.pgm";
	static final String templatePath = "/home/klein/dev/logosProject/tests/templates/scene2.png";
	static final String imagePath = "/home/klein/dev/logosProject/tests/scene.jpg";
	static final String resultImgPath = "/home/klein/dev/logosProject/tests/resultImg.jpg";
	public static void main(String[] args) {
	
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		Mat template = Imgcodecs.imread(templatePath,Imgcodecs.IMREAD_GRAYSCALE); 
		Mat scene = Imgcodecs.imread(imagePath,Imgcodecs.IMREAD_GRAYSCALE); 
		if (scene.empty() || template.empty()) {
			System.out.println("image not found");
			return;
		}
		
		KeypointsStruct templateDescripters= KeypointsExtractor.getKeypointsStruct(template);
		KeypointsStruct sceneDescripters = KeypointsExtractor.getKeypointsStruct(scene);
		
		Mat cur = templateDescripters.descriptors.row(1);
		
		
		List<MatOfDMatch> matches = new Vector<MatOfDMatch>();
		DescriptorMatcher matcher = DescriptorMatcher.create(DescriptorMatcher.FLANNBASED);
			
		matcher.knnMatch(sceneDescripters.descriptors, templateDescripters.descriptors, matches,2);
				
		
		List<DMatch> filterd2 = MatchesFilters.combinedFilters(matches);
			
		MatOfDMatch bests = new MatOfDMatch();
		bests.fromList(filterd2);
		
		Mat matchedImage = new Mat(scene.rows(), scene.cols()*2, scene.type());
		Features2d.drawMatches(scene, sceneDescripters.keypoints, template, templateDescripters.keypoints,bests , matchedImage);

		Imgcodecs.imwrite(resultImgPath, matchedImage);
	}




}
