package logosProject;

import java.util.List;
import java.util.Vector;

import org.opencv.core.Core;
import org.opencv.core.DMatch;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.Features2d;
import org.opencv.imgcodecs.Imgcodecs;

import logosProject.KeypointsExtractor.KeypointsStruct;

public class DataBaseTest {
	//static final String templatePath = "/home/klein/dev/logosProject/tests/images/box.pgm";
		//static final String imagePath = "/home/klein/dev/logosProject/tests/images/scene.pgm";
		static final String templatePath = "/home/klein/dev/logosProject/tests/templates";
		static final String imagePath = "/home/klein/dev/logosProject/tests/scene.jpg";
		static final String resultImgPath = "/home/klein/dev/logosProject/tests/resultImg.jpg";
		public static void main(String[] args) {
			System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
			
			Mat[] templates = ImageHelper.loadImages(templatePath);
			Mat scene = ImageHelper.loadImage(imagePath); 
			if (scene.empty() || templates == null) {
				System.out.println("image not found");
				return;
			}
			
			KeypointsStruct sceneDescripters = KeypointsExtractor.getKeypointsStruct(scene);
			
			List<DMatch> bestFind = null;
			KeypointsStruct bestTemplateDescripters = null;
			int largestAmount = 0;
			int index = -1;
			for(int i = 0 ; i < templates.length; i++)
			{
				Mat template = templates[i];
				KeypointsStruct templateDescripters= KeypointsExtractor.getKeypointsStruct(template);
				
				List<MatOfDMatch> matches = new Vector<MatOfDMatch>();
				DescriptorMatcher matcher = DescriptorMatcher.create(DescriptorMatcher.FLANNBASED);
					
				matcher.knnMatch(sceneDescripters.descriptors, templateDescripters.descriptors, matches,2);	
				List<DMatch> filterd2 = MatchesFilters.combinedFilters(matches);
				if(filterd2.size() > largestAmount)
				{
					largestAmount = filterd2.size();
					bestFind = filterd2;
					bestTemplateDescripters = templateDescripters;
					index = i;
				}
				
			}	
			if(bestFind != null && bestTemplateDescripters!= null)
			{
				MatOfDMatch bests = new MatOfDMatch();
				bests.fromList(bestFind);
				
				Mat matchedImage = new Mat(scene.rows(), scene.cols()*2, scene.type());
				Features2d.drawMatches(scene, sceneDescripters.keypoints, templates[index], bestTemplateDescripters.keypoints,bests , matchedImage);
	
				Imgcodecs.imwrite(resultImgPath, matchedImage);
			}
			else
				System.out.println("prob");
		}
}