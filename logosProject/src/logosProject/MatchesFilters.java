package logosProject;

import java.util.List;
import java.util.Vector;

import org.opencv.core.DMatch;
import org.opencv.core.MatOfDMatch;

public class MatchesFilters {
	
	private final static double defaultThreasholdFactor = 4;
	private final static double defaultRatio = 0.8;
	public static List<MatOfDMatch> minDistFilter(List<MatOfDMatch> matches,double threasholdFactor) {
		List<MatOfDMatch> bestMatches = new Vector<MatOfDMatch>();
		double minDist= getMinDist(matches);
		for( MatOfDMatch m :matches)
		{
			DMatch[] curr = m.toArray();
		    if( curr[0].distance <= threasholdFactor*minDist)
		    {
		    	bestMatches.add(m); 
		    }
		    
		}
		return bestMatches;
	}
	
	public static double getMinDist(List<MatOfDMatch> matches)
	{
		double minDist = Double.MAX_VALUE;
		for(MatOfDMatch m : matches)
		{
			DMatch[] curr = m.toArray();
			if(curr[0].distance < minDist)
				minDist = curr[0].distance;
		}
		return minDist;
	}
	
	public static void ratioFilter(List<MatOfDMatch> matches, List<DMatch> matchesFilteredRatio,
			final double ratio) {
		for(MatOfDMatch m : matches)
		{
			DMatch[] curr = m.toArray();
			if(curr[0].distance < ratio*curr[1].distance)
				matchesFilteredRatio.add(curr[0]);
		}
	}
	
	public static List<DMatch> combinedFilters(List<MatOfDMatch> matches,final double threasholdFactor,final double ratio) {	
		List<MatOfDMatch> matchesFiltered = minDistFilter(matches,threasholdFactor);
		List<DMatch> filterd2 = new Vector<DMatch>();
		ratioFilter(matchesFiltered, filterd2, ratio);
		return filterd2;
	}
	
	public static List<DMatch> combinedFilters(List<MatOfDMatch> matches) {
		return combinedFilters(matches,defaultThreasholdFactor,defaultRatio);
	}
	
}
