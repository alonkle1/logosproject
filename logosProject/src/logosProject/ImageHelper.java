package logosProject;

import java.io.File;
import java.util.ArrayList;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import edu.flash3388.flashlib.util.beans.ValueSource;

public class ImageHelper {
	
	public static Mat[] loadImages(String imgDirPath)
	{
		Mat[] imgs = null;
		
		File dir = new File(imgDirPath);
		if(!dir.exists() || !dir.isDirectory())
			return null;
		File[] files = dir.listFiles();
		ArrayList<Object> imgList = new ArrayList<Object>();
		for (int i = 0; i < files.length; i++) {
			Object img = loadImage(files[i].getAbsolutePath());
			if(img != null)
				imgList.add(img);
		}
		imgs = new Mat[imgList.size()];
		imgList.toArray(imgs);
		return imgs;

	}
	
	public static Mat loadImage(String imgPath) {
		return Imgcodecs.imread(imgPath,Imgcodecs.IMREAD_GRAYSCALE);
	}
}
