package communications;


public class messageHelper {
	
	public static String getCode(byte[] message)
	{
		return(getCode(new String(message)));
		
	}
	
	public static String getCode(String message)
	{
		
		if(message.length()>=3)
		{
			String code =""+message.charAt(0)+message.charAt(1)+message.charAt(2);
			if(messagesCodes.isExists(code))
				return code;
			else
				return messagesCodes.errorCode;
		}
		return(messagesCodes.errorCode);
		
	}
	
	public static String getRest(byte[] mess)
	{
		String message = new String(mess);
		return message.substring(3);
	}
	

}
