package communications;

import java.io.IOException;
import java.net.InetAddress;

import edu.flash3388.flashlib.communications.Communications;
import edu.flash3388.flashlib.communications.TcpCommInterface;

public class Server {
	
	TcpCommInterface commandServerInterface ;
	Communications commandServer;
	
	public Server(int localPort,InetAddress address,String serverName)
	{
		try {
			commandServerInterface = new TcpCommInterface(address, localPort);
			commandServer = new Communications(serverName, commandServerInterface);
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Server(int port)
	{
		
	}

}
