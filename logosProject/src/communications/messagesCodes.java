package communications;

import java.util.Map.Entry;

public class messagesCodes {
	
	public static final String recognizeRequest = "101";
	public static final String recognizeRespond = "102";
	public static final String accepted = "304";
	public static final String denied = "305";
	public static final String errorCode = "000";
	
	public static boolean isExists(String code)
	{
		return code.equals(recognizeRequest) || code.equals(recognizeRespond) || code.equals(accepted) ||code.equals(denied);
	}
	
}
